# Zotero

## Getting the latest Zotero release

How to get the *latest* tarball (not the source code)?
We can't use their Github releases page, because that's only for source code.

This works
```
curl -L "https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=5.0.66" --output zotero.tar.bz2
```
But it's not very useful, as we have to  specify the version.
Two options here:
+ drop the version argument, and the download should default to the latest version
+ get the latest version from the Github release API

Hm, the second approach above fails, for some reason.
But this seems to work:
```
curl -L "https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64" --output zotero.tar.bz2
```
Ok, seems we can get the latest tarball reliably.


## Config Zotero on new workstation by cloning its entire profile directory

Installing and configuring Zotero using the GUI is not really possible to automate.

I think the most straight-forward way to apply our config (including add-ons,
edited config files, etc.) is to clone an existing Zotero profile
*after* installing Zotero itself on the new host.

This approach has worked reliably so far.


## Zotero proxy settings

Configured proxy schema for Uppsala university:
```
%h.ezproxy.its.uu.se/%p
```
